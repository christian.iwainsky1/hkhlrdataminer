#!/bin/bash
#SBATCH -J SLURM_Example_t5_np2_nt1
#SBATCH --mail-type=ALL
#SBATCH -e /home/ci24amun/HKHLR/hkhlrdataminer/SLURM_Reference_Jobs/logs/SLURM_example_t5_np2_nt1.%j.err 
#SBATCH -o /home/ci24amun/HKHLR/hkhlrdataminer/SLURM_Reference_Jobs/logs/SLURM_example_t5_np2_nt1.%j.out
#SBATCH --mem-per-cpu=1900 -n 2
#SBATCH -t 0:05:00
#SBATCH -A hkhlr

srun -n 2 sleep 100
