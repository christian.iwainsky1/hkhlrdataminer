#!/bin/bash
#SBATCH -J SLURM_Example_t5_np4_nt4
#SBATCH --mail-type=ALL
#SBATCH -e /home/ci24amun/HKHLR/hkhlrdataminer/SLURM_Reference_Jobs/logs/SLURM_example_t5_np4_nt4.%j.err 
#SBATCH -o /home/ci24amun/HKHLR/hkhlrdataminer/SLURM_Reference_Jobs/logs/SLURM_example_t5_np4_nt4.%j.out
#SBATCH --mem-per-cpu=1900 -n 4 -c 4
#SBATCH -t 0:05:00
#SBATCH -A hkhlr

srun -n 2 sleep 100
